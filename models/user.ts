import { Schema, model} from "mongoose";
import validator from 'validator';
import Joi from 'joi';
export var UserSchema: Schema = new Schema({
    name: {
        type:String,
        required:false,
        lowercase:true,
        trim:true,
        minlength:3
    },
    lName: {
        type:String,
        required: false,
        lowercase:true,
        trim:true,
        minlength:3
    },
    email: {
        type:String,
        required: true,
        lowercase:true,
        trim:true,
        minlength:5,
        unique: true,
        validate : {
            validator: validator.isEmail,
            message : '{VALUE} is not a valid email Address! please enter a valid email address!'
        }
    },
    passwordHash: {
        type:String,
        required : true
    },
    access: [{
        type:String,
        enum: ['banned', 'savePoem','savePoem','saveCategory', 'editComment', 'adminDash','banUser', 'user'],
        required : true
    }],
    telegram: {
        type: new Schema({
            userId: {
                type:Number
            },
            username:{
                type:String,
                required: false,
                lowercase:true,
                trim:true,
                minlength:3
            },
            pageId:{
                type:Number,
                min:0,
                max:10
            },
            formSubId:{
                type: Schema.Types.ObjectId,
                ref: 'Form'
            }
        }),
        required: false
    },
    favourites:[{ type: Schema.Types.ObjectId, ref: 'Poem' }]
});

export const validate = (user:object) => {
    const schema = Joi.object().keys({
        name: Joi.string().trim().max(20).min(3).error(new Error('Name must be a string between 3 to 30 characters!')),
        lName: Joi.string().trim().min(3).max(20).error(new Error('Last name must be a string between 3 to 30 characters!')),
        email: Joi.string().email().required().error(new Error('Please enter a valid email!')),
        password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required().error(new Error('Your password must be alphanumeric and between 3-30 characters!'))
    });
    return Joi.validate(user,schema,{
        stripUnknown:true
    });
};


UserSchema.virtual('fullName').get(function () :string {
    // @ts-ignore
    return (`${this.name} ${this.lName}`);
});

export const User= model("User", UserSchema);