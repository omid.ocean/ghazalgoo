import { Schema, model} from "mongoose";
import mongoosePaginate from "mongoose-paginate";
const schema = new Schema({
    poemName: {
        type:String,
        required:true,
        lowercase:true,
        trim:true,
        minlength:3
    },
    poemDesc: {
        type:String,
        required: false,
        lowercase:true,
        trim:true,
        minlength:30
    },
    likes: {
        type:Number,
        required: false,
        default:0
    },
    parentCategory:{ type: Schema.Types.ObjectId, ref: 'Category' },
    comments:[{ type: Schema.Types.ObjectId, ref: 'Cemment' }],
    verses:[{ type: Schema.Types.ObjectId, ref: 'Verse' }]
});

schema.index({poemName : 'text'});

schema.plugin(mongoosePaginate);

export const Poem = model('Poem', schema);