import { Schema, model} from "mongoose";

const schema: Schema = new Schema({
    text: {
        type:String,
        required:true,
        lowercase:true,
        trim:true,
        minlength:3
    },
    description: {
        type:String,
        required: false,
        lowercase:true,
        trim:true,
        minlength:30
    },
    order: {
        type:Number,
        required: true
    },
    isFirst: {
        type:Boolean,
        required: false,
        default:false
    },
    poem:{ type: Schema.Types.ObjectId, ref: 'Poem' }
});

schema.index({text: 'text'});

export const Verse = model("Verse", schema);