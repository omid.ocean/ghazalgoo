import { Schema, model} from "mongoose";
import mongoosePaginate from 'mongoose-paginate';
// @ts-ignore
// Joi.objectId = require('joi-objectid')(Joi);

const schema = new Schema({
    title: {
        type:String,
        required:true,
        lowercase:true,
        trim:true,
        minlength:3,
        maxlength:50
    },
    description: {
        type:String,
        required: false,
        lowercase:true,
        trim:true,
        minlength:30
    },
    isParent: {
        type: Boolean,
        default: false
    },
    parent: {type: Schema.Types.ObjectId, ref: 'Category'},
    subCategories:[{ type: Schema.Types.ObjectId, ref: 'Category' }],
    poet:{ type: Schema.Types.ObjectId, ref: 'Poet' },
    poems:[{ type: Schema.Types.ObjectId, ref: 'Poem' }]
});

schema.plugin(mongoosePaginate);

schema.index({title : 'text'});


export const Category=  model('Category', schema);