// classes for common response structures
import {__} from 'i18n';
export class ErorResponse {
    private errorCode:number;
    private _errorMessage:string;

    constructor(error : InternalErrorCodes, message?:string) {
        this.errorCode = error;
        this._errorMessage = message ? __(message) : __(error.toString());
    }

    response () {
        return { code : this.errorCode, message : this._errorMessage}
    }

    responseHeader(): number {
        let res = HttpStatus.ServerError;
        switch (this.errorCode) {
            case InternalErrorCodes.PageOutOfBounds:
                res = HttpStatus.BadRequesst;
                break;
            case InternalErrorCodes.InvalidObjectId:
                res = HttpStatus.BadRequesst;
                break;
            case InternalErrorCodes.NotFound:
                res = HttpStatus.NotFound;
                break;
            case InternalErrorCodes.ShortQuery:
                res = HttpStatus.BadRequesst;
                break;
            case InternalErrorCodes.EmailAlreadyExists:
                res = HttpStatus.Conflict;
                break;
            case InternalErrorCodes.UnHandled:
                res = HttpStatus.ServerError;
                break;
            case InternalErrorCodes.Validator:
                res = HttpStatus.BadRequesst;
                break;
            case InternalErrorCodes.InvalidEmail:
                res = HttpStatus.BadRequesst;
                break
        }
        return res;
    }
}

export enum HttpStatus {
    OK =200,
    Created = 201,
    NoContent = 204,
    NotModifies = 304,
    BadRequesst = 400,
    Unathurized = 401,
    Forbidden = 403,
    NotFound = 404,
    Conflict = 409,
    ServerError = 500
}

export enum InternalErrorCodes {
    PageOutOfBounds = 1001,
    InvalidObjectId = 1002,
    NotFound = 1003,
    ShortQuery = 1003,
    EmailAlreadyExists = 1004,
    UnHandled = 1005,
    Validator = 1006,
    InvalidEmail = 1007
}

