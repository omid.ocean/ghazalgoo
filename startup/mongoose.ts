import mongoose from 'mongoose';

const initDatabase = () => {
    mongoose.Promise = global.Promise;
    mongoose.set('useCreateIndex', true);
    if (process.env.MONGODB_URI) mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true });
    else process.exit(1);
    require('../models/category');
    require('../models/poet');
    require('../models/poem');
    require('../models/verse')
};

export {
    mongoose,
    initDatabase
};