import {Router} from "express";
import {Poem} from "../models/poem";
import {logger} from "../startup/logging";
import {toNumber,validateId} from "../tools/Tools";
import {ErorResponse, InternalErrorCodes} from "../tools/responses";
import i18n from 'i18n';

const router = Router();

router.get('/', async (req,res)=> {
    if (!req.query.page)  req.query.page = 1;
    if (!req.query.limit) req.query.limit = 10;
    i18n.setLocale(req.locale);
    if (req.query.search && req.query.search.length < 3) {
        const err = new ErorResponse(InternalErrorCodes.ShortQuery);
        return res.status(err.responseHeader()).send(err.response())
    }
    const search = req.query.search? { $text : { $search : req.query.search } } : {};
    try {
        const poems = await Poem.paginate(search,{page : toNumber(1,req.query.page), limit: toNumber(5,req.query.limit,50), select: '-verses -comments -parentCategory'});
        if (poems.page && poems.pages && (poems.page > poems.pages || poems.page < 1))  {
            const err = new ErorResponse(InternalErrorCodes.PageOutOfBounds);
            return res.status(err.responseHeader()).send(err.response())
        }
        res.send({
            poems : poems.docs,
            total : poems.total,
            limit : poems.limit,
            page : poems.page,
            pages : poems.pages
        });
    }catch (e) {
        logger.error(e);
    }
});

router.get('/:id',async (req,res) => {
    i18n.setLocale(req.locale);
    if (!validateId(req.params.id)) {
        const err = new ErorResponse(InternalErrorCodes.InvalidObjectId);
        return res.status(err.responseHeader()).send(err.response())
    }
    //todo add pagination to poems and categories
    const poem = await Poem
        .findById(req.params.id)
        .populate('verses','order isFirst text')
        .populate('parentCategory', 'title');

    //todo populate comments after creating comments api
    if (poem) res.send({poem});
    else {
        const err = new ErorResponse(InternalErrorCodes.NotFound);
        return res.status(err.responseHeader()).send(err.response())
    }
});

//todo add post route afer adding auth!

export default router;